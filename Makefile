# Append thrift to the suffixes to watch
.SUFFIXES: .thrift

# Add some variables
# Using built in function ends in a slash
CURRENT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
# Using bash function does not but is not as portable
#CURRENT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

THRIFTC_TARGETS = py.thriftc go.thriftc cpp.thriftc
THRIFT_FILES = $(CURRENT_DIR)test.thrift $(CURRENT_DIR)test2.thrift
PY_DIR = $(CURRENT_DIR)py/gen-py
GO_DIR = $(CURRENT_DIR)go/gen-go
CPP_DIR = $(CURRENT_DIR)cpp/gen-cpp

# These targets don't correspond to files
.PHONY: gen_thrift clean $(THRIFTC_TARGETS)

# default is first target
gen_thrift: $(THRIFTC_TARGETS)

clean:
	rm -rf $(PY_DIR) $(CPP_DIR) $(GO_DIR)

py.thriftc: $(PY_DIR)/testy/ttypes.py

go.thriftc: $(GO_DIR)/testy/test.go

cpp.thriftc: $(CPP_DIR)/test_types.cpp

# For each lang, pick one file that is built.  Since not every file is rebuilt on every change, if the
# thrift file is newer than file you picked, remove all generated code for that language and rebuild
#
# use $^ for all prereqs with a space between, see
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

$(PY_DIR)/testy/ttypes.py: $(THRIFT_FILES)
	rm -rf $(PY_DIR)
	mkdir -p $(PY_DIR)
	for f in $^; do \
		thrift --gen py -o $(PY_DIR)/.. $$f ; \
	done

$(GO_DIR)/testy/test.go: $(THRIFT_FILES)
	rm -rf $(GO_DIR)
	mkdir -p $(GO_DIR)
	for f in $^; do \
		thrift --gen go -o $(GO_DIR)/.. $$f ; \
	done

$(CPP_DIR)/test_types.cpp: $(THRIFT_FILES)
	rm -rf $(CPP_DIR)
	mkdir -p $(CPP_DIR)
	for f in $^; do \
		thrift --gen cpp -o $(CPP_DIR)/.. $$f ; \
	done

dump-makefile:
	$(MAKE) -p > makefile-p