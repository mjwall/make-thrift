namespace * testy

const i32 x = 42

struct Word {
	1: required string word
	2: optional string part
}

service WordGenerator {

    Word newWord(),
    Word newe4Word()

}
